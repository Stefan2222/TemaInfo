// Suma elementelor alfate sub diagonala principala si elementele aflate sub diagonala.
#include <iostream>
using namespace std;
int main()
{
   int n,i,j,a[20][20],s=0;
   cout<<"n=";cin>>n;
   for(i=1;i<=n;i++)
      for(j=1;j<=n;j++)
      {
         cout<<"a["<<i<<"]["<<j<<"]=";
         cin>>a[i][j];
      }
   cout<<"Matricea este:"<<endl;
   for(i=1;i<=n;i++)
   {
      for(j=1;j<=n;j++)
         cout<<a[i][j]<<" ";
      cout<<endl;
   }
   cout<<"Elementele aflate sub diagonala principala sunt:"<<endl;
   for (i=1;i<=n;i++)
   {
      for (j=1;j<=n;j++)
         if (j<=i)
         {
            cout<<a[i][j]<<" ";
            s=s+a[i][j];
         }
         else
            cout<<"  ";
      cout<<endl;
   }
   cout<<"Suma elementelor aflate sub diagonala principala este s="<<s<<endl;
   return 0;
}
